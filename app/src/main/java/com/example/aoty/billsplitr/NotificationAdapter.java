package com.example.aoty.billsplitr;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


/**
 * Created by robin on 05-Jan-18.
 */

public class NotificationAdapter extends ArrayAdapter<Notification> {
    public NotificationAdapter(@NonNull Context context, Notification[] notifications){
        super(context,R.layout.custom_notificationrow,notifications);
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.custom_notificationrow, parent,false);
        TextView notificationTitle = (TextView)customView.findViewById(R.id.NotificationRowTitle);
        TextView notificationWriter = (TextView)customView.findViewById(R.id.NotifcationRowWriter);
        Notification notification = getItem(position);
        notificationTitle.setText(notification.getTitle());
        notificationWriter.setText(notification.getSubmittedBy().getFullName());

        return customView;
    }
}
