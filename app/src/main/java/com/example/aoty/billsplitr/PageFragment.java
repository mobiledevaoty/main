package com.example.aoty.billsplitr;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class PageFragment extends android.support.v4.app.Fragment {
    TextView textView;

    public PageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.page_fragment_layour,container,false);
        textView = (TextView)view.findViewById(R.id.FragmentTextView);
        ListView listView = (ListView)view.findViewById(R.id.FragmentListView) ;
        Bundle bundle = getArguments();
        int value = bundle.getInt("count");
        final int groupID = bundle.getInt("groupID");
        String member = bundle.getString("name");

        DummyMemberRepository appRep = new DummyMemberRepository();
        final List<Group> groupsList = appRep.getGroups();
        Group group = groupsList.get(groupID);
        List<Bills> bills = group.getBills();
        Bills[]billsArray = new Bills[bills.size()];
        bills.toArray(billsArray);

        List<Member> members = group.getMembers();
        Member[]membersArray = new Member[members.size()];
        members.toArray(membersArray);

        List<Notification> notifications = group.getNotifications();
        Notification[]notificationsArray = new Notification[notifications.size()];
        notifications.toArray(notificationsArray);



        switch(value){
            case 0:
                textView.setText("Members:");
                final ListAdapter memberListAdapter = new MemberAdapter(this.getContext(),membersArray );
                listView.setAdapter(memberListAdapter);
                listView.setOnItemClickListener(
                        new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(parent.getContext(), MemberDetail.class);
                                intent.putExtra("int_value", position);
                                intent.putExtra("groupId", groupID);
                                startActivity(intent);
                            }
                        }
                );
                break;

            case 1:
                textView.setText("Bills:");
                final ListAdapter billListAdapter = new BillsAdapter(this.getContext(), billsArray, member);
                listView.setAdapter(billListAdapter);
                listView.setOnItemClickListener(
                        new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(parent.getContext(), BillDetail.class);
                                intent.putExtra("int_value", position);
                                intent.putExtra("groupId", groupID);
                                startActivity(intent);
                            }
                        }
                );

                break;

            case 2:
                textView.setText("Notifications:");
                final  ListAdapter notificationListAdapter = new NotificationAdapter(this.getContext(), notificationsArray);
                listView.setAdapter(notificationListAdapter);
                listView.setOnItemClickListener(
                        new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(parent.getContext(), NotificationDetail.class);
                                intent.putExtra("int_value", position);
                                intent.putExtra("groupId", groupID);
                                startActivity(intent);
                            }
                        }
                );
                break;


        }


        return view;
    }

}
