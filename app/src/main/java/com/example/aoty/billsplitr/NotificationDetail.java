package com.example.aoty.billsplitr;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class NotificationDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_detail);

        DummyMemberRepository appRep = new DummyMemberRepository();
        Intent intent = getIntent();
        int groupId = intent.getIntExtra("groupId", -1);
        int position = intent.getIntExtra("int_value",-1);

        Group group = appRep.getGroups().get(groupId);
        List<Notification> notificationList = group.getNotifications();
        Notification notification =  notificationList.get(position);
        Member submitter = notification.getSubmittedBy();

        TextView title = (TextView) findViewById(R.id.NotificationDetailTitle);
        TextView description = (TextView)findViewById(R.id.NotificationDetailDescription);
        TextView notificationSubmitter = (TextView) findViewById(R.id.NotificationDetailSubmitter);
        TextView notificationSubmitted = (TextView) findViewById(R.id.NotificationDetailSubmitted);
        ImageView imageView = (ImageView) findViewById(R.id.NotificationDetailImage);

        title.setText(notification.getTitle());
        description.setText(notification.getDescription());
        notificationSubmitted.setText(notification.getDay() + "/" + notification.getMonth() + "/" + notification.getYear() + " " + notification.getHour() + ":" +  notification.getMinute());
        notificationSubmitter.setText(submitter.getFullName());

        switch(submitter.getMemberName()){
            case "Robin":
                imageView.setImageResource(R.drawable.robin);
                break;
            case "Midas":
                imageView.setImageResource(R.drawable.midas);
                break;
            case "Stan":
                imageView.setImageResource(R.drawable.stan);
            case "Dirk":
                imageView.setImageResource(R.drawable.dirk);
                break;
            case "Mr.":
                imageView.setImageResource(R.drawable.bean);
                break;
        }
    }
}
