package com.example.aoty.billsplitr;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by robin on 05-Jan-18.
 */

public class SwipeAdapter extends FragmentStatePagerAdapter {
    int GroupID;
    String member;

    public SwipeAdapter(FragmentManager fm, int GroupID,String newMember) {

        super(fm);
        this.GroupID = GroupID;
        this.member = newMember;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new PageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("count",position);
        bundle.putInt("groupID", GroupID);
        bundle.putString("name",member);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
