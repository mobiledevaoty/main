package com.example.aoty.billsplitr;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;



import org.w3c.dom.Text;

import java.util.List;

public class MemberDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);

        DummyMemberRepository appRep = new DummyMemberRepository();
        Intent intent = getIntent();
        int groupId = intent.getIntExtra("groupId", -1);
        int position = intent.getIntExtra("int_value",-1);

        Group group = appRep.getGroups().get(groupId);
        List<Member> memberList = group.getMembers();
        final Member member =  memberList.get(position);

        ImageView Button = (ImageView)findViewById(R.id.imageButton1);
        Button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://www.facebook.com/" + member.getFacebook()));
                startActivity(intent);
            }
        });

        ImageView Button2 = (ImageView)findViewById(R.id.imageButton2);
        Button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://www.twitter.com/" + member.getTwitter()));
                startActivity(intent);
            }


        });

        ImageView Button3 = (ImageView)findViewById(R.id.imageButton3);
        Button3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://www.instagram.com/" +member.getInstagram() ));
                startActivity(intent);
            }


        });




        TextView firstName = (TextView) findViewById(R.id.firstName);
        TextView lastName = (TextView)findViewById(R.id.lastName);
        TextView firstAdres = (TextView) findViewById(R.id.adress);
        TextView secondAdres = (TextView) findViewById(R.id.city);
        TextView phoneNumber = (TextView) findViewById(R.id.phoneNumber);
        TextView dob = (TextView)findViewById(R.id.dob);
        ImageView imageView = (ImageView) findViewById(R.id.profilePic);

        firstName.setText(member.getMemberName());
        lastName.setText(member.getFamilyName());
        firstAdres.setText(member.getStreet() +" " +String.valueOf(member.getNr()));
        secondAdres.setText(String.valueOf(member.getZipCode()) + " " + member.getCity());
        phoneNumber.setText(member.getPhoneNumber());
        dob.setText(String.valueOf(member.getDay()) + "/" + member.getMonth()+ "/" + member.getYear());
        switch(member.getMemberName()){
            case "Robin":
                imageView.setImageResource(R.drawable.robin);
                break;
            case "Midas":
                imageView.setImageResource(R.drawable.midas);
                break;
            case "Stan":
                imageView.setImageResource(R.drawable.stan);
                break;
            case "Dirk":
                imageView.setImageResource(R.drawable.dirk);
                break;
            case "Mr.":
                imageView.setImageResource(R.drawable.bean);
                break;
    }
    }
}
