package com.example.aoty.billsplitr;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by robin on 05-Jan-18.
 */

public class MemberAdapter extends ArrayAdapter<Member> {
    public MemberAdapter(@NonNull Context context, Member[] members){
        super(context,R.layout.custom_memberrow,members);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.custom_memberrow, parent,false);
        TextView memberName = customView.findViewById(R.id.MemberRowName);
        ImageView memberPicture = customView.findViewById(R.id.MemberRowPicture);

        Member member = getItem(position);
        memberName.setText(member.getFullName());

        switch(member.getMemberName()){
            case "Robin":
                memberPicture.setImageResource(R.drawable.robin);
                break;
            case "Midas":
                memberPicture.setImageResource(R.drawable.midas);
                break;
            case "Dirk":
                memberPicture.setImageResource(R.drawable.dirk);
                break;
            case "Stan":
                memberPicture.setImageResource(R.drawable.stan);
                break;
            case "Mr.":
                memberPicture.setImageResource(R.drawable.bean);
                break;
        }

        return customView;
    }
}
