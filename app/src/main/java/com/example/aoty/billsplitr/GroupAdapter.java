package com.example.aoty.billsplitr;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


/**
 * Created by robin on 05-Jan-18.
 */

class GroupAdapter extends ArrayAdapter<Group>{
    public GroupAdapter(@NonNull Context context, Group[] groups, String newMemberName) {
        super(context,R.layout.custom_grouprow, groups);
        memberName = newMemberName;
    }
    String memberName;
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.custom_grouprow, parent, false);
        Bitmap bitmap;
        ImageView imageView = (ImageView)customView.findViewById(R.id.GroupAdapterGroupImage);
        TextView groupName = (TextView) customView.findViewById(R.id.GroupAdapterGroupName);
        TextView groupNotification = (TextView) customView.findViewById(R.id.GroupAdapterGroupNotification);
        TextView groupValue = (TextView) customView.findViewById(R.id.GroupAdapterValue);

        Group group = getItem(position);
        String GroupName = group.getGroupName();
        String GroupNotification = group.getNotifications().get(0).getTitle();
        String groupPictureName =  group.getPictureName();

        List<Bills> bills = group.getBills();
        double toPay = 0;


        for( int j = 0; j< bills.size();j++) {
                Bills bill = bills.get(j);

                List<Member> payingMembers = bill.getPayingMembers();
                List<Integer> payPercentage = bill.getPayPercentage();

                if(bill.getOriginalBuyer().getMemberName() == memberName){
                    for (int i = 0; i < payingMembers.size(); i++) {
                        Member tempMember = payingMembers.get(i);
                        if (tempMember.getMemberName() == memberName) {
                            int percentage = 100 - payPercentage.get(i);
                            double value = bill.getPrice();
                            toPay -= (value * ((double)percentage / 100));
                        }

                    }
                }
                else{
                for (int i = 0; i < payingMembers.size(); i++) {
                    Member tempMember = payingMembers.get(i);
                    if (tempMember.getMemberName() == memberName) {
                            int percentage = payPercentage.get(i);
                            double value = bill.getPrice();
                            toPay += (value * ((double)percentage / 100));
                        }

                    }
                }

        }


        switch(groupPictureName){
            case "Midas":
                imageView.setImageResource(R.drawable.midas);
                break;

            case "Robin":
                imageView.setImageResource(R.drawable.robin);
                break;

            case "Mr.":
                imageView.setImageResource(R.drawable.bean);
                break;
            case "Dirk":
                imageView.setImageResource((R.drawable.dirk));
                break;
            case "Stan":
                imageView.setImageResource(R.drawable.stan);
        }


        groupName.setText(GroupName);
        groupNotification.setText(GroupNotification);

            if (toPay>0){
                groupValue.setTextColor(Color.RED);
                groupValue.setText("- € " + String.valueOf(Math.round(toPay*100.0)/100.0));
            }

            else if(toPay <= 0){
                groupValue.setTextColor(Color.GREEN);
                if(toPay<0){
                    groupValue.setText("€ " + String.valueOf(Math.abs(toPay)));
                }
                else groupValue.setText(String.valueOf(toPay));
            }
    return customView;
    }
}
