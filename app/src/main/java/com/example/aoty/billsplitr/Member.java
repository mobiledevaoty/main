package com.example.aoty.billsplitr;


import java.time.LocalDate;
import java.util.Calendar;

/**
 * Created by Midasvg on 12/28/2017.
 */

public class Member {
    private  int id;

    /* Date Registered */
    private int day;
    private int month;
    private int year;


    private  String memberName;
    private String twitter;



    private String facebook;
    private String instagram;

    private String familyName;
    private String city;
    private String street;
    private int number;
    private String phoneNumber;
    private int zipCode;

    private int age;


    public Member(int id, int day, int month, int year,  String memberName, String familyName, String city, int zipCode, String street, int number, String phoneNumber, String Facebook, String Instagram, String Twitter){
        this.id = id;
        this.day = day;
        this.month = month;
        this.year = year;
        this.twitter = Twitter;
        this.facebook = Facebook;
        this.instagram = Instagram;
        this.memberName = memberName;
        this.familyName = familyName;
        this.city = city;
        this.zipCode = zipCode;
        this.phoneNumber = phoneNumber;
        this.street = street;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String contactName) {
        this.memberName = contactName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }


    public String getFullName(){return memberName + " " + familyName;}


    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNr() {
        return number;
    }

    public void setNr(int number) {
        this.number = number;
    }


    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }





}
