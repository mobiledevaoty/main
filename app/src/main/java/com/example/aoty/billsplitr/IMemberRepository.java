package com.example.aoty.billsplitr;


/**
 * Created by Midasvg on 12/28/2017.
 */
import java.lang.reflect.Member;
import java.util.List;

public interface IMemberRepository {
    List<com.example.aoty.billsplitr.Member> getMember();
}
