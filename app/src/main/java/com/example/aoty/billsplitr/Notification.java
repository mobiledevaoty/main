package com.example.aoty.billsplitr;

/**
 * Created by Midasvg on 12/28/2017.
 */

public class Notification {
    private  int id;

    private  String title;
    private  String description;

    private int day;
    private int month;
    private int year;
    private int hour;
    private int minute;

    private int bedrag;

    private Member submittedBy;

    public Notification(int id,String title, String description, int day, int month, int year, int hour, int minute, int bedrag, Member submittedBy){
        this.id = id;
        this.title = title;
        this.description = description;

        this.day = day;
        this.month = month;
        this.year = year;
        this.hour = hour;
        this.minute = minute;
        this.bedrag = bedrag;
        this.submittedBy = submittedBy;

    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String  getTitle(){return title;}
    public void setTitle(String title){this.title = title;}

    public String  getDescription(){return description;}
    public void setDescription(String description){this.description = description;}

    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public int getHour() {
        return hour;
    }
    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }
    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getBedrag() {
        return bedrag;
    }
    public void setBedrag(int bedrag) {
        this.bedrag = bedrag;
    }

    public Member  getSubmittedBy(){return submittedBy;}
    public void setSubmittedBy(Member submittedBy){this.submittedBy = submittedBy;}






}
