package com.example.aoty.billsplitr;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class GroupDetail extends FragmentActivity {
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);
        viewPager = (ViewPager)findViewById(R.id.view_pager);
        int groupid = getIntent().getIntExtra("int_value" , -1);
        String member = getIntent().getStringExtra("name");
        TextView textView = (TextView)findViewById(R.id.GroupDetailName);
        TextView Notification = (TextView)findViewById(R.id.GroupDetailNotification);

        DummyMemberRepository appRep = new DummyMemberRepository();
        final List<Group> groupsList = appRep.getGroups();
        Group group = groupsList.get(groupid);
        textView.setText(group.getGroupName());
        Notification.setText(group.getLastNotification());
        SwipeAdapter swipeAdapter = new SwipeAdapter(getSupportFragmentManager(),groupid,member);

        viewPager.setAdapter(swipeAdapter);

    }
}
