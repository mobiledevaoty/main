package com.example.aoty.billsplitr;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Midasvg on 12/28/2017.
 */

public class DummyMemberRepository implements IMemberRepository {
    private  static IMemberRepository repo = null;
    public  static IMemberRepository getInstance(){
        if (repo ==null){
            repo = new DummyMemberRepository();
        }
        return  repo;
    }

    public List<com.example.aoty.billsplitr.Member> getMember(){
        List<com.example.aoty.billsplitr.Member> users = new ArrayList<>();
        users.add(new Member(1,4,8,1998,"Robin","Laevaert","Londerzeel",1840,"Holle Eikstraat", 8, "0478946907", "robin.laevaert", "robinlaevaert", "platinaguy"));
        users.add(new Member(2,10,4,1998,"Midas","Van Gucht","Puurs",2870,"Straat",9,"0477056729", "midas.vangucht", "midasvangucht", "midasvg"));
        users.add(new Member(3,16,2,1996,"Dirk","Porrez","Antwerpen",2000,"Falconplein",9,"0478978571", "Dirk-Porrez-553974438009989","dirkporrez", "dirkporrez"));
        users.add(new Member(3,16,2,1996,"Stan","Zwetsloot","Londerzeel",1840,"Falconplein",9,"0478967547", "stan.zwetsloot", "robbehuysmans", "JagexJed"));
        users.add(new Member(3,16,2,1996,"Mr.","Bean","London",8000,"Teddystreet",5,"025847151", "MrBean","mrbean", "MrBean"));
        return  users;
    }

    public List<Notification> getNotification(){
        List<Notification> notifications = new ArrayList<>();
        return  notifications;
    }

    public List<Bills> getBills(){

        List<com.example.aoty.billsplitr.Member> users = new ArrayList<>();
        users.add(new Member(1,4,8,1998,"Robin","Laevaert","Londerzeel",1840,"Holle Eikstraat", 8, "0478946907", "robin.laevaert", "robinlaevaert", "platinaguy"));
        users.add(new Member(2,10,4,1998,"Midas","Van Gucht","Puurs",2870,"Straat",9,"0477056729", "midas.vangucht", "midasvangucht", "midasvg"));
        users.add(new Member(3,16,2,1996,"Dirk","Porrez","Antwerpen",2000,"Falconplein",9,"0478978571", "Dirk-Porrez-553974438009989","dirkporrez", "dirkporrez"));
        users.add(new Member(3,16,2,1996,"Stan","Zwetsloot","Londerzeel",1840,"Falconplein",9,"0478967547", "stan.zwetsloot", "robbehuysmans", "JagexJed"));
        users.add(new Member(3,16,2,1996,"Mr.","Bean","London",8000,"Teddystreet",5,"025847151", "MrBean","mrbean", "MrBean"));

        List<Integer> payPercentage1 = new ArrayList<>();
        payPercentage1.add(50);
        payPercentage1.add(50);
        payPercentage1.add(0);
        payPercentage1.add(0);
        payPercentage1.add(0);

        List<Integer> payPercentage2 = new ArrayList<>();
        payPercentage2.add(70);
        payPercentage2.add(30);
        payPercentage2.add(0);
        payPercentage2.add(0);
        payPercentage2.add(0);

        List<Bills> bills = new ArrayList<>();
        bills.add(new Bills(1,50,4,1,2017,"Colruyt","Drank feestje",users.get(0),users,payPercentage1));
        bills.add(new Bills(2,70,8,1,2017,"Media Markt","Games",users.get(1),users,payPercentage2));
        bills.add(new Bills(1,58.40,4,1,2017,"Elektriciteitsrekening","Maandelijkse Betaling",users.get(1),users,payPercentage1));

        return bills;
    }

    public List<Group> getGroups(){

        List<com.example.aoty.billsplitr.Member> users = new ArrayList<>();
        users.add(new Member(1,4,8,1998,"Robin","Laevaert","Londerzeel",1840,"Holle Eikstraat", 8, "0478946907", "robin.laevaert", "robinlaevaert", "platinaguy"));
        users.add(new Member(2,10,4,1998,"Midas","Van Gucht","Puurs",2870,"Straat",9,"0477056729", "midas.vangucht", "midasvangucht", "midasvg"));
        users.add(new Member(3,16,2,1996,"Dirk","Porrez","Antwerpen",2000,"Falconplein",9,"0478978571", "Dirk-Porrez-553974438009989","dirkporrez", "dirkporrez"));
        users.add(new Member(3,16,2,1996,"Stan","Zwetsloot","Londerzeel",1840,"Falconplein",9,"0478967547", "stan.zwetsloot", "robbehuysmans", "JagexJed"));
        users.add(new Member(3,16,2,1996,"Mr.","Bean","London",8000,"Teddystreet",5,"025847151", "MrBean","mrbean", "MrBean"));

        List<Integer> payPercentage1 = new ArrayList<>();
        payPercentage1.add(50);
        payPercentage1.add(50);
        payPercentage1.add(0);
        payPercentage1.add(0);
        payPercentage1.add(0);

        List<Integer> payPercentage2 = new ArrayList<>();
        payPercentage2.add(70);
        payPercentage2.add(15);
        payPercentage2.add(15);
        payPercentage2.add(0);
        payPercentage2.add(0);

        List<Integer> payPercentage3 = new ArrayList<>();
        payPercentage3.add(40);
        payPercentage3.add(30);
        payPercentage3.add(30);
        payPercentage3.add(0);
        payPercentage3.add(0);

        List<Bills> bills = new ArrayList<>();
        bills.add(new Bills(1,50,4,1,2017,"Colruyt","Drank feestje",users.get(0),users,payPercentage1));
        bills.add(new Bills(2,170,8,1,2017,"Media Markt","Games",users.get(1),users,payPercentage2));

        List<Bills> bills2 = new ArrayList<>();
        bills2.add(new Bills(1,50,4,1,2017,"Colruyt","Drank feestje",users.get(1),users,payPercentage1));
        bills2.add(new Bills(2,170,8,1,2017,"Media Markt","Games",users.get(0),users,payPercentage2));

        List<Bills> bills3 = new ArrayList<>();
        bills3.add(new Bills(1,58.40,4,1,2017,"Elektriciteitsrekening","Maandelijkse Betaling",users.get(1),users,payPercentage1));
        bills3.add(new Bills(2,899,8,1,2017,"Media Markt","Nieuwe televisie",users.get(0),users,payPercentage3));

        List<Notification> notifications = new ArrayList<>();
        notifications.add(new Notification(1,"Wc papier nodig","Er zijn nog maar 2 rollen WC-papier!!",3,1,2017,19,46,0,users.get(1)));
        notifications.add(new Notification(1,"Werken aan het huis","Er zitten vochtplekken op de muren en er zijn 3 lampen stuk. Wie kan dit fixen?",3,1,2017,19,46,0,users.get(0)));
        notifications.add(new Notification(1,"Kast monteren","Ik heb een nieuwe kast gekocht maar deze moet nog in elkaar gestoken worden.",3,1,2017,19,46,0,users.get(2)));
        notifications.add(new Notification(1,"Pakje aannemen","Er wordt morgen een pakje geleverd maar ik ben niet thuis om het aan te nemen. Wie wel?",3,1,2017,19,46,0,users.get(3)));


        List<Notification> notifications2 = new ArrayList<>();
        notifications2.add(new Notification(1,"Eten moet gehaald worden","Er zijn nog maar 2 rollen WC-papier!!",3,1,2017,19,46,0,users.get(0)));
        notifications2.add(new Notification(1,"Werken aan het huis","Er zitten vochtplekken op de muren en er zijn 3 lampen stuk. Wie kan dit fixen?",3,1,2017,19,46,0,users.get(0)));
        notifications2.add(new Notification(1,"Kast monteren","Ik heb een nieuwe kast gekocht maar deze moet nog in elkaar gestoken worden.",3,1,2017,19,46,0,users.get(2)));
        notifications2.add(new Notification(1,"Pakje aannemen","Er wordt morgen een pakje geleverd maar ik ben niet thuis om het aan te nemen. Wie wel?",3,1,2017,19,46,0,users.get(3)));


        List<Notification> notifications3 = new ArrayList<>();
        notifications3.add(new Notification(1,"Werken aan het huis","Er zitten vochtplekken op de muren en er zijn 3 lampen stuk. Wie kan dit fixen?",3,1,2017,19,46,0,users.get(0)));
        notifications3.add(new Notification(1,"Kast monteren","Ik heb een nieuwe kast gekocht maar deze moet nog in elkaar gestoken worden.",3,1,2017,19,46,0,users.get(2)));
        notifications3.add(new Notification(1,"Pakje aannemen","Er wordt morgen een pakje geleverd maar ik ben niet thuis om het aan te nemen. Wie wel?",3,1,2017,19,46,0,users.get(3)));

        List<Group> groups = new ArrayList<>();
        groups.add(new Group(1,1,1,2017,users,"Party!",notifications,bills,"Mr." ));
        groups.add(new Group(2,1,8,2017,users,"Security",notifications2,bills2,"Dirk" ));
        groups.add(new Group(3,8,1,2018,users, "KotSquad",notifications3,bills3, "Robin" ));
        groups.add(new Group(4,16,1,2018,users, "Thuis",notifications3,bills3, "Midas" ));
        groups.add(new Group(5,16,1,2018,users, "Finland",notifications,bills, "Dirk" ));
        groups.add(new Group(6,1,1,2017,users,"Werk",notifications,bills,"Robin" ));
        groups.add(new Group(7,1,8,2017,users,"Voetbalclub",notifications2,bills2,"Mr." ));
        groups.add(new Group(8,8,1,2018,users, "Werk2",notifications3,bills3, "Midas" ));
        groups.add(new Group(9,16,1,2018,users, "Familiefeest",notifications3,bills3, "Robin" ));
        groups.add(new Group(10,16,1,2018,users, "Reisje",notifications,bills, "Dirk" ));
        return groups;
    }
}
