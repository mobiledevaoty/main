package com.example.aoty.billsplitr;

import android.os.Parcelable;

import java.net.URL;
import java.util.List;

/**
 * Created by robin on 05-Jan-18.
 */

public class Group  {

    /* id */
    private int id;

    /* Date Registered */
    private int day;
    private int month;
    private int year;


    private List<Member> members;
    private String groupName;
    private List<Notification> notifications;
    private List<Bills> bills;
    private String pictureName;

    public Group(int id, int day, int month, int year, List<Member> members, String groupName, List<Notification> notifications, List<Bills> bills, String pictureName) {
        this.id = id;
        this.day = day;
        this.month = month;
        this.year = year;
        this.members = members;
        this.groupName = groupName;
        this.notifications = notifications;
        this.bills = bills;
        this.pictureName = pictureName;
    }


    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String imageURL) {
        this.pictureName = imageURL;
    }

    public int getId(){return id;}
    public void setId(int id){this.id = id;}
    public List<com.example.aoty.billsplitr.Member> getMembers(){return members;}
    public void setMembers(List<com.example.aoty.billsplitr.Member> members){this.members = members;}
    public String getGroupName(){return groupName;}
    public void setGroupName(String groupName){this.groupName = groupName;}
    public List<Notification> getNotifications(){return notifications;}
    public void setNotifications(List<Notification> notifications){this.notifications = notifications;};
    public List<Bills> getBills(){return bills;}
    public void setBills(List<Bills> bills){this.bills = bills;}

    public String getLastNotification(){
        String lastNotification;
        if(this.notifications != null && !notifications.isEmpty()){
            lastNotification = (notifications.get(notifications.size()-1)).getTitle();
        }
        else lastNotification = "";
        return lastNotification;
    }
}
