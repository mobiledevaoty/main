package com.example.aoty.billsplitr;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by robin on 05-Jan-18.
 */

public class BillsAdapter extends ArrayAdapter<Bills> {
    public BillsAdapter(@NonNull Context context, Bills[] bills, String newMemberName) {
        super(context, R.layout.custom_billsrow, bills);
        memberName = newMemberName;
    }
    String memberName;

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.custom_billsrow, parent,false);

        TextView totalPrice = (TextView)customView.findViewById(R.id.BillRowTotal);
        TextView toPayview = (TextView)customView.findViewById(R.id.BillRowToPay);
        TextView store = (TextView)customView.findViewById(R.id.BillRowStore);
        TextView title = (TextView)customView.findViewById(R.id.BillRowTitle);

        Bills bill = getItem(position);
        String Store = bill.getStore();
        String Title = bill.getDescription();
        double TotalPrice = bill.getPrice();
        double ToPay = 0;

        List<Member> Members = bill.getPayingMembers();
        List<Integer> Percentages = bill.getPayPercentage();

        ToPay = bill.getToPay(memberName);

        /*for(int i = 0; i<Members.size();i++){
            if(Members.get(i).getMemberName() == "Robin"){
                ToPay = TotalPrice * ((Percentages.get(i))/100);
            }
        }*/

        store.setText(Store);
        title.setText(Title);
        totalPrice.setText("€ " + String.valueOf(TotalPrice));
        /*if(ToPay>0){
        toPayview.setTextColor(Color.RED);
        toPayview.setText("- € " + String.valueOf(ToPay));
        }
        else if(ToPay <=0){
            toPayview.setTextColor(Color.GREEN);
            if(ToPay<0){
                toPayview.setText("€ " + String.valueOf(Math.abs(ToPay)));
            }
            else toPayview.setText(String.valueOf(ToPay));
        }*/
        toPayview.setText(String.valueOf(ToPay));

        return customView;
    }
}
