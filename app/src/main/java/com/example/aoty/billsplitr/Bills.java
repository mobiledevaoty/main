package com.example.aoty.billsplitr;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.List;

public class Bills extends AppCompatActivity {

    public Bills(int id, double price, int day, int month, int year, String store, String description, Member originalBuyer, List<Member> payingMembers, List<Integer> payPercentage) {
        this.id = id;
        this.price = price;
        this.day = day;
        Month = month;
        Year = year;
        this.store = store;
        this.description = description;
        this.originalBuyer = originalBuyer;
        this.payingMembers = payingMembers;
        this.payPercentage = payPercentage;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bills);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    private int id;

    private double price;
    private int day;
    private int Month;
    private int Year;

    private String store;
    private String description;
    private Member originalBuyer;
    private List<Member> payingMembers;
    private List<Integer> payPercentage;


    public String getDate(){String value = String.valueOf(this.day) + "/" + String.valueOf(this.Month) + "/" + String.valueOf(this.Year);
    return value;}
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Member getOriginalBuyer() {
        return originalBuyer;
    }

    public void setOriginalBuyer(Member originalBuyer) {
        this.originalBuyer = originalBuyer;
    }

    public List<Member> getPayingMembers() {
        return payingMembers;
    }

    public void setPayingMembers(List<Member> payingMembers) {
        this.payingMembers = payingMembers;
    }

    public List<Integer> getPayPercentage() {
        return payPercentage;
    }

    public void setPayPercentage(List<Integer> payPercentage) {
        this.payPercentage = payPercentage;
    }

    public int getToPay(String MemberName){
        int toPay = 0;
        String memberName = MemberName;
        List<Member> Members = this.getPayingMembers();
        List<Integer> PayPercentages = this.getPayPercentage();
        if(this.getOriginalBuyer().getMemberName() == memberName) {
            for (int i = 0; i < Members.size(); i++) {
                Member tempMember = payingMembers.get(i);
                if (tempMember.getMemberName() == memberName) {
                    int percentage = 100 - payPercentage.get(i);
                    double value = this.getPrice();
                    toPay -= (value * ((double)percentage / 100));
                }
            }
        }
        else{
            for (int i = 0; i < payingMembers.size(); i++) {
                Member tempMember = payingMembers.get(i);
                if (tempMember.getMemberName() == memberName) {
                    int percentage = payPercentage.get(i);
                    double value = this.getPrice();
                    toPay += (value * ((double)percentage / 100));
                }

            }

        }

        return toPay;
    }







}
