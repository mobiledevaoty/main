package com.example.aoty.billsplitr;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.List;

public class GroupsList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);
        final String member = getIntent().getStringExtra("member_name");

        DummyMemberRepository appRep = new DummyMemberRepository();
        final List<Group> groupsList = appRep.getGroups();
        final Group[]groupsArray = new Group[groupsList.size()];
        groupsList.toArray(groupsArray);
        final ListAdapter groupListAdapter = new GroupAdapter(this,groupsArray,member);
        ListView groupListView = (ListView) findViewById(R.id.groupList);
        groupListView.setAdapter(groupListAdapter);

        groupListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(GroupsList.this,GroupDetail.class);
                        //Group group = groupsList.get(position);
                        intent.putExtra("int_value", position);
                        intent.putExtra("name",member);
                        startActivity(intent);
                    }
                }
        );


    }
}
