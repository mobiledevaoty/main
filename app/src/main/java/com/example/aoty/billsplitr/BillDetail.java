package com.example.aoty.billsplitr;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class BillDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_detail);
        PieChart chart = (PieChart) findViewById(R.id.chart);
        TextView titleView = (TextView) findViewById(R.id.BillDetailTitle);
        TextView descriptionView = (TextView) findViewById(R.id.BillDetailDescription);
        TextView buyerView = (TextView) findViewById(R.id.BillDetailBuyer);
        TextView dateView = (TextView) findViewById(R.id.BillDetailDate);

        DummyMemberRepository appRep = new DummyMemberRepository();
        Intent intent = getIntent();
        int groupId = intent.getIntExtra("groupId", -1);
        int position = intent.getIntExtra("int_value",-1);

        Bills Bill  = appRep.getGroups().get(groupId).getBills().get(position);
        String title = Bill.getStore();
        String billdescription = Bill.getDescription();
        String buyer = Bill.getOriginalBuyer().getFullName();
        String date = Bill.getDate();


        Legend legend = chart.getLegend();
        legend.setEnabled(true);
        legend.setXEntrySpace(5f);
        Description description = new Description();
        description.setText("");

        chart.setDrawSliceText(false);

        List<PieEntry> datalijst = new ArrayList<PieEntry>();
        List<Member> payingMembers = Bill.getPayingMembers();
        List<Integer> paypercent = Bill.getPayPercentage();

        for(int i = 0; i < payingMembers.size();i++){
            if(paypercent.get(i) != 0)
            datalijst.add(new PieEntry(paypercent.get(i),payingMembers.get(i).getMemberName()));
        }

        PieDataSet dataSet = new PieDataSet(datalijst, "");
        dataSet.setColors(new int[]{Color.RED,Color.YELLOW,Color.GREEN,Color.BLUE,Color.CYAN,Color.GRAY,Color.MAGENTA,Color.rgb(255,154,0),Color.rgb(255,0,119),Color.rgb(204,255,153)});
        legend.setWordWrapEnabled(true);
        chart.setDescription(description);
        PieData pieData = new PieData(dataSet);
        chart.setCenterText("€ " + String.valueOf(Bill.getPrice()));
        chart.setData(pieData);
        chart.invalidate();
        titleView.setText(title);
        descriptionView.setText(billdescription);
        buyerView.setText(buyer);
        dateView.setText(date);


    }






    }




